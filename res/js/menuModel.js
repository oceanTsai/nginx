/**
 *  auth ocean
 *  
 */
(function(){
	if(!window.pageModel){
		window.pageModel = {};
	}

	var menuModel = {		
		ko : null,
		container : null,
		viewModel : null,
		setResource : function( resource ){
			this.viewModel.resource( resource );
		},
		getResource : function(){
			return this.viewModel.resource();
		},
		pushItem : function( item ){
			this.viewModel.resource.push( item );
		},
		popItem : function(){
			this.viewModel.resource.pop();
		},
		removeAll : function(){
			this.viewModel.resource.removeAll();
		},
		excuteBind : function(){			
			
			this.viewModel = {
				resource : ko.observableArray() //雙向綁定
			};
			
			ko.applyBindings( this.viewModel, this.container );
		},
		init : function( ko, container ){			
			this.ko = ko;
			this.container = container;
			this.excuteBind();
		}
	};

	window.pageModel.menuModel = menuModel;	
	var container = document.getElementById('navContainer');	
	menuModel.init( window.ko, container);
})();