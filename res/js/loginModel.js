/**
 *  auth ocean
 *  此模組用來控制 login按鈕的 渲染、特效，
 *  由knockoutjs bind data,
 *  只要由數值修改方法設定資料即可自動更改view。
 *  
 */
(function(){

	if(!window.pageModel){
		window.pageModel = {};
	}

	var loginModel = {
		service : null,		//ajax service		
		ko : null,			//knockoutjs 主體
		container : null,	//knickoutjs View對象		
		viewModel : null,   //knockoutjs ViewModel
		accountView : null, //帳號組件
		pwdView : null,		//密碼元件		
		logInBtn : null,
		laddaView : null,
		subjects : {}, //event 
		//事件名稱列舉
		EVENT_LOGIN_SUCCESS : 'event_login_success' ,
		EVENT_LOGOUT_SUCCESS : 'event_logout_success' ,
		EVENT_LOGIN_FAIL : 'event_login_fail',
		EVENT_LOGOUT_FAIL : 'event_logout_fail',
		//加入一個訂閱者
		addSubject : function( eventType, notifyFunction ){
			if(  this.subjects[ eventType ] ){
				throw ' eventType repeat ! '
			}else{
				this.subjects[ eventType ] = notifyFunction;				
			}			
		},
		//移除一個訂閱者
		removeSubject : function( eventType ){
			if( this.subjects[ eventType ] ){
				this.subjects[ eventType ] = null;
				delete this.subjects[ eventType ] ;
			}
		},
		//發送通知
		notify : function( eventType ){
			for ( var type in this.subjects){
				if( eventType == type){
					this.subjects[ type ]();
				}
			}
		},
		setButton : function(view){
			this.loginBtn = $(view);
			this.laddaView = Ladda.create( view );
			this.loginBtn.bind('click' , this.logBtnClickHandler );
		},
		logBtnClickHandler : function(){
			var that = loginModel;		
			that.startEffect();			
			( that.getIsLogin() ) ?  that.logOut() : that.logIn() ;			
			that = null;			
		},
		//loading effect
		startEffect :  function(){			
			this.laddaView.start();					
		},
		stopEffect : function(){
			this.laddaView.stop();
		},
		getPassword : function(){
			return this.pwdView.val();
		},
		getAccount : function(){
			return this.accountView.val();
		},
		catchAccount : function(){			
			var account = this.getAccount();
			var catchValue = $.cookie('account');
			if(catchValue){
				var valList = catchValue.split(',');
				var isExist = false;
				for(var i=0,size=valList.length ; i < size ; i++){
					if(valList[i] == account){
						isExist = true;
					}
				}
				catchValue = (isExist) ? catchValue :   catchValue + ',' +account ;

			}else{
				catchValue = account;				
			}

			$.cookie('account', catchValue,  { expires: 7 });			
		},
		logIn : function(){			
			var account = this.getAccount();
			var pwd = this.getPassword();
			if(account && pwd){							
				this.service.logIn( this.logInSuccess, this.logInFail, this, account, pwd );	
			}else{
				this.stopEffect();
				alert('請輸入帳號與密碼!');
			}
		},
		logInSuccess : function(data, textStatus, jqXHR ){
			this.target.stopEffect();
			if( data!=null && data.result!=null ){					
				if(data.result.user && data.result.success){
					this.target.catchAccount();
					this.target.notify( this.target.EVENT_LOGIN_SUCCESS );
				}else{
					this.target.notify( this.target.EVENT_LOGIN_FAIL );	
				}
			}else{
				this.target.notify( this.target.EVENT_LOGIN_FAIL );
			}

			this.target = null; //clear memory
		},
		logInFail : function(){
			this.target.stopEffect();
			this.target.notify( this.target.EVENT_LOGIN_FAIL );
			this.target = null;
		},
		logOut : function(){			
			this.service.logOut( this.logOutSucess, this.logOutFail, this );
		},
		logOutSucess : function(){
			this.target.stopEffect();
			this.target.notify( this.target.EVENT_LOGOUT_SUCCESS );
		},
		logOutFail : function(){
			this.target.stopEffect();
			this.target.notify( this.target.EVENT_LOGOUT_FAIL );
		},
		//boolean 是否在登入狀態
		setIsLogin : function(value){
			this.viewModel.isLogIn(value);
			this.stopEffect();
		},
		getIsLogin : function(){
			return this.viewModel.isLogIn();
		},
		//將data與view做bind，往後只要呼叫setIsLogin( ) 更改數值，就會更改view。
		excuteBind : function(){
			
			this.viewModel = {
				isLogIn : ko.observable() //雙向綁定				
			};
			//判斷out in 呈現文字
			this.viewModel.logStatusTxt = ko.computed(function() {
        		return this.isLogIn()  ? "Sign out" : "Sign in";
    		}, this.viewModel);
			
			ko.applyBindings( this.viewModel, this.container );
		},
		init : function(service, ko, container , logInBtn, pwdView, accountView){			
			this.service = service;
			this.ko = ko;
			this.container = container;
			this.setButton( logInBtn );
			this.pwdView = pwdView;
			this.accountView = accountView;
			this.excuteBind();
		}
	};

	window.pageModel.loginModel = loginModel;
	var service = window.pageModel.service;
	var container = document.getElementById( 'loginContainer' );	
	loginModel.init(service, window.ko, container, document.getElementById( 'loginBtn' ), $('#pwdView'), $('#accountView') );

})();