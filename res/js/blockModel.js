/**
 *  auth ocean
 *  
 */
(function(){
	if(!window.pageModel){
		window.pageModel = {};
	}

	var blockModel = {		
		ko : null,
		container : null,
		viewModel : null,		
		opt : {
					//defaults.baseZ = 9999;	
					message: '<div></div>', 
					css: {
							border: 'none', 
			        		padding: '15px', 
			        		backgroundColor: '#000', 
			        		'-webkit-border-radius': '10px', 
			        		'-moz-border-radius': '10px', 
			        		opacity: .6, 
			        		color: '#fff'
			        	}
		},
		//loading mask
		startLoading : function(path){
			$.blockUI(this.opt);
		},
		stopLoading : function(){
			$.unblockUI(); 
		},	
		init : function(message){			
			this.opt.message = message;
		}
	};
	window.pageModel.blockModel = blockModel;	
})();