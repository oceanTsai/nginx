(function(){
	if(!window.pageModel){
		window.pageModel = {};			
	}
	 var service = {
			 	jsonAjax : function(url, type ,success, fail, target, data){
			 		$.ajax({  
				        type: type,  
				        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',  
				        url: url,  
				        dataType: 'json', 
				        data : data,
				        success: success,
				        error: fail,
				        target : target
				        });
			 	},
			 	getResource : function(success, fail, target){
				 	this.jsonAjax('/englishProject/service/resource', 'GET', success, fail, target, null);
			 	},
			 	logIn : function(success, fail, target, mail, pwd){
			 		var data = {mail : mail , pwd : pwd};
			 		this.jsonAjax('/englishProject/service/login', 'POST', success, fail, target, data);	
			 	},
			 	logOut : function(success, fail, target){
			 		this.jsonAjax('/englishProject/service/logout', 'POST', success, fail, target, null);	
			 	},
			 	insertSentence : function(
			 		success, fail , target, content, context_tw, context_cn, sound, icon, category){
			 		var data = {
			 			content : content,
			 			context_tw : context_tw,
			 			context_cn : context_cn,
			 			sound : sound,
			 			icon : icon,
			 			category : category
			 		};
			 		this.jsonAjax('/englishProject/sentence/insert', 'POST', success, fail, target, data);
			 	},
			 	getSentenceCategory : function(success, fail , target){
			 		this.jsonAjax('/englishProject/sentence/category', 'GET', success, fail, target);
			 	},
			 	//取詞句API，具取分頁資料功能
			 	pagingSentence : function(success, fail, target, skip, count, find, sort){	
			 		var findStr='';
			 		if(find && find != ''){
			 			findStr = JSON.stringify(find);
			 		}
			 		var sortStr = '';
			 		if(sort && sort != ''){
			 			sortStr = JSON.stringify(sort);
			 		}
			 		var data = {
			 			skip : skip,
			 			count : count,
			 			find :findStr,
			 			sort : sortStr
			 		};

			 		this.jsonAjax('/englishProject/sentence/paging', 'GET', success, fail, target, data);
			 	},
			 	//取最新的n筆資料
			 	getTopSentence : function(success, fail, target, count){			 		
			 		this.pagingSentence(success, fail, target, 0, count, null,  {modifiedDate:-1} );
			 	},
			 	updateSentence : function(success, fail, target, id, content, context_tw, context_cn, sound, icon, category){
			 		var data = {
			 			id : id,
			 			content : content,
			 			context_tw : context_tw,
			 			context_cn : context_cn,
			 			sound : sound,
			 			icon : icon,
			 			category : category
			 		};
			 		this.jsonAjax('/englishProject/sentence/update', 'POST', success, fail, target, data);
			 	},
			 	getRandomSentence : function(success, fail, target){
			 		this.jsonAjax('/englishProject/service/randomFind', 'GET', success, fail, target, null);
			 	}
		};
		window.pageModel.service = service;
})();